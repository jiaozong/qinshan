const { defineConfig } = require('@vue/cli-service')
console.log(process, '----');
module.exports = defineConfig({
	transpileDependencies: true,
	lintOnSave: false,
	publicPath: './',
	// assetsPublicPath: '/kn-front/',
	outputDir: process.env.VUE_OUT_PUT_DIR,
	productionSourceMap: false,
	indexPath: 'index .html',
	assetsDir: 'static',
	devServer: {
		port: 3000,
		https: false,
		open: true, // 配置自动启动浏览器
		proxy: {
			'/api': {
				target: "http://qsfod01.cnnp.com.cn:8088",//http://218.13.91.107:36000 //http://192.168.2.3:8889
				ws: true,
				changeOrigin: true,
			},
			'/portal-apis': {
				target: "http://218.13.91.107:33009",
				ws: true,
				changeOrigin: true,
			},
		},
	},
	configureWebpack: {
		resolve: {
			alias: {
				'vue$': 'vue/dist/vue.esm.js'
			}
		}
	},
	chainWebpack: config => {
		config
			.plugin('html')
			.tap(args => {
				args[0].title = 'sie-人才'
				return args
			})
		config.module
			.rule('scss')
			.oneOf('vue')
			.use('px2rem-loader')
			.loader('px2rem-loader')
			.before('postcss-loader') // this makes it work.
			.options({ remUnit: 192, remPrecision: 8 })
			.end()

		const oneOfsMap = config.module.rule('scss').oneOfs.store
		oneOfsMap.forEach(item => {
			item
				.use('sass-resources-loader')
				.loader('sass-resources-loader')
				.options({
					// 写入定义基础样式的scss文件路径
					resources: [
						'./src/styles/variables.scss',
					]
				})
				.end()
		})

	},
})
