// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store';
import pageApi from '@/api';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css'; //element css
import VueClipboard from 'vue-clipboard2';
import './utils/flexible';
Vue.config.productionTip = false;
Vue.prototype.$store = store;
import * as echarts from 'echarts';
Vue.prototype.echarts = echarts;

Vue.prototype.api = pageApi;
Vue.use(ElementUI, { size: 'small' });
Vue.use(VueClipboard);
/* eslint-disable no-new */

Vue.directive('selectLoadmore', {
  bind(el, binding) {
    // 获取element-ui定义好的scroll盒子
    const SELECTWRAP_DOM = el.querySelector('.el-select-dropdown .el-select-dropdown__wrap');
    SELECTWRAP_DOM.addEventListener('scroll', function () {
      /**
      * scrollHeight 获取元素内容高度(只读)
      * scrollTop 获取或者设置元素的偏移值,常用于, 计算滚动条的位置, 当一个元素的容器没有产生垂直方向的滚动条, 那它的scrollTop的值默认为0.
      * clientHeight 读取元素的可见高度(只读)
      * 如果元素滚动到底, 下面等式返回true, 没有则返回false:
      * ele.scrollHeight - ele.scrollTop === ele.clientHeight;
      */
      const condition = this.scrollHeight - this.scrollTop <= this.clientHeight;
      if (condition) {
        binding.value();
      }
    });
  }
});
/*window.onresize = setHtmlFontSize;
function setHtmlFontSize(){
    const htmlWidth = document.documentElement.clientWidth || document.body.clientWidth;
    const htmlDom = document.getElementsByTagName('html')[0];
    htmlDom.style.fontSize = htmlWidth / 10 + 'px';
};
setHtmlFontSize(); */
//当前视口宽度
let nowClientWidth = document.documentElement.clientWidth;
//换算方法
function nowSize(val,initWidth=1920){
    return val * (nowClientWidth/initWidth);
}
Vue.prototype.$nowSize = nowSize;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app")