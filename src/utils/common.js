/**
 * @param {string} path
 * @returns {Boolean}
 */

export function setState(state) {
  switch(state) {
    case 0: return '未开始'; 
    case 1: return '进行中';
    case 2: return '已结束';
  }
}

/*
DEPT_MANAGER("DEPT_MANAGER","部门领导"),
    BRCH_MANAGER("BRCH_MANAGER" , "分管领导"),
    SCHOOLS_HR("SCHOOLS_HR","院校人事负责人100020"),
    GROUP_HRBP("GROUP_HRBP","集团人事负责人100030"),
    OTHER("OTHER" , "其他员工"),

*/
export const roleList = ['DEPT_MANAGER','BRCH_MANAGER','SCHOOLS_HR','GROUP_HRBP','OTHER']