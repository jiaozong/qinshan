import router from '@/router';

//进度条
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
import store from '@/store';
NProgress.configure({ showSpinner: false }); // 进度条配置不显示右上角环形加载
import { getLocalStorage } from '@/utils/cache';

const white_list = ['/login', '/create-plan', '/personal-report']; // 没有重定向的白名单
const loginCenter = process.env.LOGIN_CENTER;
// 路由拦截
router.beforeEach(async (to, from, next) => {
	NProgress.start();
	const token = getLocalStorage('X-Token');
	// if (token) {
	// 	if (to.path === '/login') {
	// 		next({ path: '/' });
	// 		NProgress.done();
	// 	} else {
	// 		const hasGetUserInfo = store.getters.userInfo;
	// 		if (Object.entries(hasGetUserInfo).length) {
	// 			next();
	// 		} else {
	// 			try {
	// 				await store.dispatch('user/getUserInfo');
	// 				const accessRoutes = await store.dispatch('permission/generateRoutes');
	// 				console.log(accessRoutes)
	// 				accessRoutes.push({
	// 					path: '*',
	// 					redirect: '/404',
	// 					hidden: true,
	// 				});

	// 				for (let i = 0, length = accessRoutes.length; i < length; i += 1) {
	// 					const element = accessRoutes[i];
	// 					router.addRoute(element);
	// 				}

	// 				router.options.isAddAsyncMenuData = true;

	// 				next({ ...to, replace: true });
	// 			} catch (error) {
	// 				//console.log('error', error);
	// 				next({ loginCenter , replace: true });
	// 				// next(`/login?redirect=${to.path}`);
	// 				NProgress.done();
	// 			}
	// 		}
	// 	}
	// } else {
	// 	if (white_list.indexOf(to.path) !== -1) {
	// 		next();
	// 		NProgress.done();
	// 	} else {
	// 		alert("123213")
	// 		//next(`/login?redirect=${to.path}`);
	// 		NProgress.done();
	// 	}
	// }
	next();
});

router.afterEach(() => {
	NProgress.done();
});
