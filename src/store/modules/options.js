//import { talentRouter, constantRoutes } from '@/router';

const state = {
	stageList: [
    { label: '全部', value: 0 },
    { label: '指标收集', value: 1 },
    { label: '指标审核', value: 2 },
    { label: '指标反馈', value: 3 },
    { label: '指标评价', value: 4 },
  ],
	statusList: [
    { label: '已发布', value: 1 },
    { label: '草稿', value: 0 }
  ],
  frequencyList: [
    {label: '周', value: '周'},
    {label: '月', value: '月'},
    {label: '季', value: '季'},
    {label: '半年', value: '半年'},
    {label: '年', value: '年'},
  ],
  plannedIndicatorsList: [
    { label: '进行中', value: 1 },
    { label: '已结束', value: 2 }
  ],
  planStatus: [ // 计划状态
    // { label: '未开始', value: 0 },
    { label: '进行中', value: 1 },
    { label: '已结束', value: 2 },
  ]

};

const mutations = {

};

const actions = {

};

export default {
	namespaced: true,
	state,
	mutations,
	actions,
};
