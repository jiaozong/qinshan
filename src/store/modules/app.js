const state = {
	isCollapse: false, //左边导航栏是否收起
	drawer: false, //左侧导航栏弹窗
	// 主色调系列
	$dt_a: '#41b883',
	$dt_b: '#41b8838a',
	$dt_c: '#41b88300',
	// 副色调系列
	$vt_a: '#35495e',
	$vt_b: '#35495e83',
	$vt_c: '#35495e00',
};

const mutations = {
	upAside(state, val) {
		state.isCollapse = val != undefined ? val : !state.isCollapse;
	},
};

export default {
	namespaced: true,
	state,
	mutations,
};
