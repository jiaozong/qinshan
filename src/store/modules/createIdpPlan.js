//import { talentRouter, constantRoutes } from '@/router';

const state = {
    /**第一步 基本信息 */
    baseInfo: {},
};

const mutations = {
    setBaseInfo: (state, payload) => {
        state.baseInfo = payload;
    },
};

const actions = {};
const getters = {
    baseInfo: (state) => state.baseInfo,
};

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters,
};
