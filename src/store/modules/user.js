import api from '@/api';

const state = {
	userInfo: {},
};

const mutations = {
	set_baseInfo: (state, payload) => {
		console.log('set_baseInfo', payload);
		state.userInfo = payload;
	},
};

const actions = {
	getUserInfo({ commit, state }) {
		return new Promise((resolve, reject) => {
			api.baseInfo.baseInfoApi.getUserInfo().then((res) => {
				let data = res.data ? res.data : null;
				commit('set_baseInfo', data);
				resolve(data);
			});
		});
	},
};

export default {
	namespaced: true,
	state,
	mutations,
	actions,
};
