const getters = {
	userInfo: (state) => state.user.userInfo,
	frequencyList: (state) => state.options.frequencyList,
	permission_routes: (state) => state.permission.routes,
	cachedViews: (state) => state.tagsView.cachedViews,
};
export default getters;
