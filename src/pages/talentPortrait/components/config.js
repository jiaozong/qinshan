import { USABLE } from "./options";

// 工作经历
export const mainPostColumns = function () {
  return [
    {
      prop: "BEGDA",
      label: "开始日期",
      type: "date",
      width: "150px",
    },
    {
      prop: "ENDDA",
      label: "结束日期",
      type: "date",
      width: "150px",
    },
    {
      prop: "ZZARBGB",
      label: "工作单位",
    },
    {
      prop: "ZZDEPTN",
      label: "所在部门",
    },
    {
      prop: "ZZJOB",
      label: "职务",
      //  width: "140px",
    },
    {
      prop: "ZLINGYU_TEXT",
      label: "领域",
    },
    {
      prop: "ZZHUANYE_TEXT",
      label: "专业",
    },
    {
      prop: "ZGZSC",
      label: "年限",
    },
    {
      type: 'solt',
      slotName: "options",
      label: "操作",
      //  width: "140px",
    },
  ];
};

// 教育经历
export const eduExperienceColumns = function () {
  return [
    {
      prop: "BEGDA",
      label: "入学日期",
    },
    {
      prop: "ENDDA",
      label: "毕业日期",
    },
    {
      prop: "XLSTEXT",
      label: "学历",
    },
    {
      prop: "ZEHR_XWTXT",
      label: "学位",
    },
    {
      prop: "ZEHR_SCNAM",
      label: "学校名称",
    },
    {
      prop: "ZEHR_FACH",
      label: "专业",
    },
    {
      prop: "ZEHR_XWNAM",
      label: "学位名称",
    },
    {
      type: 'solt',
      slotName: "options",
      label: "操作",
      //  width: "140px",
    },
  ];
};

// 通讯信息
export const newsInfoColumns = function () {
  return [
    {
      prop: "USRTY",
      label: "通讯类型",
    },
    {
      prop: "USRID",
      label: "联系方式",
    },
    {
      prop: "ZSJYYS",
      label: "手机运营商",
    },
    {
      type: 'solt',
      slotName: "options",
      label: "操作",
    },
  ];
};

// 职业资格
export const professionalColumns = function () {
  return [
    {
      prop: "BEGDA",
      label: "开始日期",
      type: "date",
      width: "140px",
    },
    {
      prop: "ENDDA",
      label: "结束日期",
      type: "date",
      width: "140px",
    },
    {
      prop: "ZZCLASS",
      label: "资质类别",
    },
    {
      prop: "QNAME_TXT",
      label: "资质名称",
    },
    {
      prop: "QNAME_TXT2",
      label: "资质名称2",
    },
    {
      prop: "QLVL_TXT",
      label: "资格级别",
    },
    {
      prop: "ZZDATE",
      label: "到期日期",
    },
    {
      type: 'solt',
      slotName: "options",
      label: "操作",
    },
  ];
};
//职业聘任
export const careerAppointmentColumns = function () {
  return [
    {
      prop: "BEGDA",
      label: "开始日期",
      width: "140px",
    },
    {
      prop: "ENDDA",
      label: "结束日期",
      width: "140px",
    },
    {
      prop: "ZZCLASS",
      label: "资质类别",
    },
    {
      prop: "QNAME_TXT2",
      label: "资质名称2",
    },
    {
      prop: "ZZPRDP",
      label: "聘任单位",
    },
    {
      prop: "PRLX",
      label: "聘任类型",
    },
    {
      prop: "ZZPRNO",
      label: "聘任文号",
    },
    {
      type: 'solt',
      slotName: "options",
      label: "操作",
    },
  ];
};

//荣誉信息
export const honorInfoColumns = function () {
  return [
    {
      prop: "ZYYMC",
      label: "荣誉名称",
    },
    {
      prop: "ZHJRQ",
      label: "获奖时间",
      type: "date",
      width: "140px",
    },
    {
      prop: "ZFZJG",
      label: "发证机构",
    },
    {
      prop: "ZCJ",
      label: "等级",

    },
    {
      type: 'solt',
      slotName: "options",
      label: "操作",
      width: "140px",
    },
  ];
};

// 员工考核信息
export const examineInfoColumns = function () {
  return [
    {
      prop: "BEGDA",
      label: "开始日期",
      type: "date",
      width: "140px",
    },
    {
      prop: "ENDDA",
      label: "结束日期",
      type: "date",
      width: "140px",
    },
    {
      prop: "DSUBT",
      label: "考核类型",
    },
    {
      prop: "ZZRESULT",
      label: "考核结果",
    },
    {
      type: 'solt',
      slotName: "options",
      label: "操作",
      width: "140px",
    },
  ];
};

// 地址信息
export const addressInfoColumns = function () {
  return [
    {
      prop: "ANSSA",
      label: "地址类型",
    },
    {
      prop: "ZZLAND",
      label: "国家",
    },
    {
      prop: "BEZEI20",
      label: "省份",
    },
    {
      prop: "PAD_ORT01",
      label: "城市/地区",
    },
    {
      prop: "PAD_ORT02",
      label: "区/县",
    },
    {
      prop: "ZZSTREET",
      label: "乡镇/街道",
    },
    {
      prop: "ZZDETAIL",
      label: "详细地址",
    },
    {
      prop: "ZZPSTLZ",
      label: "邮编",
    },
    {
      type: 'solt',
      slotName: "options",
      label: "操作",
      width: "140px",
    },
  ];
};

// 家庭成员
export const emplFamlColumns = function () {
  return [
    {
      prop: "FAMSA",
      label: "名称",
    },
    {
      prop: "ZENAME",
      label: "姓名",
    },
    {
      prop: "FASEX",
      label: "性别",
    },
    {
      prop: "FGBDT",
      label: "出生日期",
    },
    {
      prop: "FGBOT",
      label: "出生地",
    },
    {
      prop: "NUM01",
      label: "手机号",
    },
    {
      prop: "ZZGZDW",
      label: "工作单位",
    },
    {
      prop: "ZZZW",
      label: "职务",
    },
    {
      prop: "TEXT50",
      label: "民族",
    },
    {
      prop: "PTEXT",
      label: "政治面貌",
    },
    {
      type: 'solt',
      slotName: "options",
      label: "操作",
      width: "140px",
    },
  ];
};

// 人才标签
export const gridColumns = function () {
  return [
    {
      prop: "signName",
      label: "标签名称",
    },
    {
      prop: "signTypeName",
      label: "标签类型",
    },
    {
      prop: "comment",
      label: "标签说明",
    },
    {
      prop: "createDate",
      label: "创建时间",
      type: "date",
      width: "140px",
    },
  ];
};

// 过失-画像
export const portraitFaultColumns = function () {
  return [
    {
      prop: "hpsRewReason",
      label: "过失名称",
      showToolTip: false,
    },
    {
      prop: "rewType",
      label: "过失等级",
      width: "140px",
    },
    {
      prop: "hpsRewdDat",
      label: "过失时间",
      type: "date",
      width: "140px",
    },
  ];
};
