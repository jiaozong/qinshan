import request from '@/api/http/request';

export const publicInfoApi = {
	/**计划列表 */
	userInfo(params) {
		return request.get('/cnnp.com~basic~f_cnnp_odatabasic_java/bpm.svc/getLoginUser()?$format=json', params);
	},
};
