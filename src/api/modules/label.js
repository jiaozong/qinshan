import request from '@/api/http/request';

export const labelApi = {
    //标签类型
    /**标签类型-分页 */
    otrsigntypePage(params) {
        return request.post('/api/otrsigntype/pageForDTO', params);
    },
    /**标签类型-获取全部启用的 */
    otrsigntypeListALl(params) {
        return request.get('/api/otrsigntype/listALl', params);
    },
    /**标签类型-保存 */
    otrsigntypeSave(params) {
        return request.post('/api/otrsigntype/save', params);
    },
    /**标签类型-修改 */
    otrsigntypeUpdate(params) {
        return request.post('/api/otrsigntype/update', params);
    },
    //标签管理
    /**标签管理-分页 */
    otrsignPage(params) {
        return request.post('/api/otrsign/pageForDTO', params);
    },
    /**标签管理-根据标签类型ID获取相关标签 */
    otrsignListBySignTypeId(params) {
        return request.get('/api/otrsign/listBySignTypeId', params);
    },
    /**标签管理-保存 */
    otrsignSave(params) {
        return request.post('/api/otrsign/save', params);
    },
    /**标签管理-修改 */
    otrsignUpdate(params) {
        return request.post('/api/otrsign/update', params);
    },
    /**标签管理-导出 */
    otrsignExport(params) {
        return request.postDownload('/api/otrsign/export', params);
    },
    /**标签管理-获取导入模板 */
    otrsignExcelExport(params) {
        return request.getDownload('/api/otrsign/excelExport', params);
    },

    //打标签
    /**打标签-分页 */
    otrsignrelationPage(params) {
        return request.post('/api/otrsignrelation/pageForDTO', params);
    },

    /**打标签-保存 */
    otrsignrelationSave(params) {
        return request.post('/api/otrsignrelation/save', params);
    },
    /**打标签-修改 */
    otrsignrelationUpdate(params) {
        return request.post('/api/otrsignrelation/update', params);
    },
    /**打标签-导出 */
    otrsignrelationExport(params) {
        return request.postDownload('/api/otrsignrelation/export', params);
    },
    /**打标签-获取导入模板 */
    otrsignrelationExcelExport(params) {
        return request.postDownload('/api/otrsignrelation/excelExport', params);
    },
};
