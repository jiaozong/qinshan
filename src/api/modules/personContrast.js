import request from '@/api/http/request';

export const personContrastApi = {
    /**查询所有对比人员列表 */
    otrcomparelistQueryAll(params) {
        return request.get('/api/otr/otrcomparelist/queryAll', params);
    },
    /**分页查询对比人员列表 */
    otrcomparelistPage(params) {
        return request.get('/api/otr/otrcomparelist/page', params);
    },
    /**单个删除人才对比清单 */
    otrcomparelistDelete(id) {
        return request.post(`/api/otr/otrcomparelist/delete/${id}`);
    },
    /**批量删除人才对比清单 */
    otrcomparelistDeleteMore(params) {
        return request.post('/api/otr/otrcomparelist/deleteMore', params);
    },
    /**新增人才对比清单 */
    otrcomparelistAdd(params) {
        return request.post('/api/otr/otrcomparelist/add', params);
    },


};
