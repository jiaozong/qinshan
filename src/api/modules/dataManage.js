import request from '@/api/http/request';

export const dataManageApi = {
    /** 电子 */
    /**档案类别 */
    archiveCategoryListApi(params) {
        return request.get(`/sap/opu/odata/sap/zbpm_h123_srv/SH_ZZIDTYPESet`);
    },
    /**档案列表-用作与全部 */
    archiveListAllApi(params) {
        return request.get(`/sap/opu/odata/sap/ZHR_DZZZGL_SRV/ZdalbSet?$filter=Objid eq'${params.Objid}'and Pernr eq '${params.Pernr}' and Ename eq '${params.Ename}'`);
    },
     /**档案列表 */
     archiveListApi(params) {
        return request.get(`/sap/opu/odata/sap/ZHR_DZZZGL_SRV/ZdalbSet?&$skip=${params.skip}&$top=${params.top}&$filter=Objid eq'${params.Objid}'and Pernr eq '${params.Pernr}' and Ename eq '${params.Ename}'`);
    },
    /**员工奖励信息 */
    empphotoSearchEmpRwd(params) {
        return request.get('/api/otr/empphoto/searchEmpRwd', params);
    },
    /**查询盘点数据 */
    empphotoSearchInventory(params) {
        return request.get('/api/otr/empphoto/searchInventory', params);
    },
    /**工作经历 */
    empphotoSearchWorkExp(params) {
        return request.get(`/sap/opu/odata/sap/ZBPM_EMPLOYEE_RESUME_INQUIRY_SRV/P0023_1Set?$filter=PERNR eq'${params.emplId}'&$format=json`);
    },
    /**教育经历 */
    eduExp(params) {
        return request.get(`/sap/opu/odata/sap/ZBPM_EMPLOYEE_RESUME_INQUIRY_SRV/P9022Set?$filter=PERNR eq '${params.emplId}'&$format=json`);
    },
   
};
