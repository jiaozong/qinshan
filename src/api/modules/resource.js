import request from '@/api/http/request';

export const resourceApi = {
	/**获取场地分页查询 */
	trainSitePageList(params) {
		return request.get('/train/site/pageList', params);
	},

	/** 保存场地 */
	saveSite(params) {
		return request.post('/train/site/save', params);
	},

	/**获取根据ID场地 */
	getFindSite(params) {
		return request.get('/train/site/find', params);
	},

	/** 更新场地数据 */
	updateSite(params) {
		return request.put('/train/site/update', params);
	},

	/**获取场地的租借安排表 */
	getTrainSiteLease(params) {
		return request.get('/train/site/lease', params);
	},

	/**获取证书分页查询 */
	trainCertificatePageList(params) {
		return request.get('/train/certificate/pageList', params);
	},

	/**根据ID获取证书详情 */
	getFindCertificate(params) {
		return request.get('/train/certificate/find', params);
	},

	/**获取获得证书列表 */
	getCertificatePageList(params) {
		return request.get('/train/certificate/getCertificatePageList', params);
	},

	/** 更新证书 */
	updateCertificate(params) {
		return request.put('/train/certificate/update', params);
	},

	/** 保存证书 */
	saveCertificate(params) {
		return request.post('/train/certificate/save', params);
	},

	/**获取讲师分页查询 */
	trainTeacherPageList(params) {
		return request.get('/train/teacher/pageList', params);
	},

	/**获取课程管理列表 */
	getCoursePageList(params) {
		return request.get('/train/course/pageList', params);
	},

	/** 创建课程 */
	saveCourse(params) {
		return request.post('/train/course/save', params);
	},

	/** 更新课程 */
	updateCourse(params) {
		return request.post('/train/course/update', params);
	},

	/**根据ID获取课程数据 */
	findCourse(params) {
		return request.get('/train/course/find', params);
	},

	/** 创建讲师 */
	saveTeacher(params) {
		return request.post('/train/teacher/save', params);
	},

	/**根据ID获取课程数据 */
	findTeacher(params) {
		return request.get('/train/teacher/find', params);
	},
	/**获取授课记录 */
	getTeachingRecords(params) {
		return request.get('/train/teacher/teachingRecords', params);
	},

	/**培训机构分页列表 */
	institutionPageList(params) {
		return request.get('/train/institution/institutionPageList', params);
	},

	/**培训机构添加保存 */
	institutionAdd(params) {
		return request.post('/train/institution/institutionAdd', params);
	},

	/**培训机构详情 */
	institutionInfo(params) {
		return request.get('/train/institution/institutionInfo', params);
	},

	/**培训机构更新保存 */
	institutionUpdate(params) {
		return request.post('/train/institution/institutionUpdate', params);
	},
};
