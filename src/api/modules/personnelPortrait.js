import request from '@/api/http/request';

export const personnelPortraitApi = {
	/**分页查询模型库表 */
	otrmodelPageModel(params) {
		return request.get('/api/otr/otrmodel/pageModel', params);
	},
    /**保存模型库 */
	otrmodelAddOrUpdate(params) {
		return request.post('/api/otr/otrmodel/addOrUpdate', params);
	},
    /**获取模型库表详情信息 */
	otrmodelQueryDetails(id) {
		return request.get(`/api/otr/otrmodel/queryDetails/${id}`);
	},
    /**获取所有模型 */
	otrmodelListAll(params) {
		return request.get('/api/otr/otrmodel/listAll', params);
	},
    /**分页查询人才模型配置表 */
	otrempmodelPage(params) {
		return request.get('/api/otr/otrempmodel/page', params);
	},
	/**保存人才模型指标 */
	otrempmodelSaveIndex(params) {
		return request.post('/api/otr/otrempmodel/saveIndex', params);
	},
	/**获取人才模型配置表详情信息 */
	otrempmodelDetails(params) {
		return request.get('/api/otr/otrempmodel/details', params);
	},
	
};
