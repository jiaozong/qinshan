import request from '@/api/http/request';

export const searchPersonApi = {
    /**分页查询人员信息 */
    otrempqueryPageQuery(params) {
        return request.get('/api/otr/otrempquery/pageQuery', params);
    },
    /**获取民族列表 */
    otrempqueryGetEthnicGrpCd(params) {
        return request.get('/api/otr/otrempquery/getEthnicGrpCd', params);
    },
     /**查询所有字典数据 */
     xitemListAll(params) {
        return request.get('/api/dm/xitem/listAll', params);
    },

};
