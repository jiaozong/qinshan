import request from '@/api/http/request';

export const talentPortraitApi = {
    /**查询基本信息 */
    empphotoSearchBaseInfo(params) {
        return request.get(`/sap/opu/odata/sap/ZBPM_EMPLOYEE_RESUME_INQUIRY_SRV/BasicInfoSet(PERNR='${params.emplId}')?$format=json`);
    },
    /**查询人才标签 */
    empphotoSearchLabel(params) {
        return request.get(`/sap/opu/odata/sap/ZBPM_EMPLOYEE_RESUME_INQUIRY_SRV/YGHX_BQSet?$filter=PERNR eq'${params.emplId}'&$format=json`);
    },
    /**员工奖励信息 */
    empphotoSearchEmpRwd(params) {
        return request.get('/api/otr/empphoto/searchEmpRwd', params);
    },
    /**查询盘点数据 */
    empphotoSearchInventory(params) {
        return request.get('/api/otr/empphoto/searchInventory', params);
    },
    /**工作经历 */
    empphotoSearchWorkExp(params) {
        return request.get(`/sap/opu/odata/sap/ZBPM_EMPLOYEE_RESUME_INQUIRY_SRV/P0023_1Set?$filter=PERNR eq'${params.emplId}'&$format=json`);
    },
    /**教育经历 */
    eduExp(params) {
        return request.get(`/sap/opu/odata/sap/ZBPM_EMPLOYEE_RESUME_INQUIRY_SRV/P9022Set?$filter=PERNR eq '${params.emplId}'&$format=json`);
    },
    /**通讯信息 */
    newsInfoApi(params) {
        return request.get(`/sap/opu/odata/sap/ZBPM_EMPLOYEE_RESUME_INQUIRY_SRV/P0105Set?$filter=PERNR eq '${params.emplId}'&$format=json`);
    },
    /**职业资格 */
    professionalApi(params) {
        return request.get(`/sap/opu/odata/sap/ZBPM_EMPLOYEE_RESUME_INQUIRY_SRV/P9003Set?$filter=PERNR eq '${params.emplId}'&$format=json`);
    },
    /**职业聘任 */
    careerAppointmentApi(params) {
        return request.get(`/sap/opu/odata/sap/ZBPM_EMPLOYEE_RESUME_INQUIRY_SRV/P9028Set?$filter=PERNR eq '${params.emplId}'&$format=json`);
    },
    /**获取荣誉信息 */
    honorInfoApi(params) {
        return request.get(`/sap/opu/odata/sap/ZBPM_EMPLOYEE_RESUME_INQUIRY_SRV/P9041Set?$filter=PERNR eq '${params.emplId}'&$format=json`);
    },
    /**员工考核信息 */
    examineInfoApi(params) {
        return request.get(`/sap/opu/odata/sap/ZBPM_EMPLOYEE_RESUME_INQUIRY_SRV/P9004Set?$filter=PERNR eq '${params.emplId}'&$format=json`);
    },
    /**家庭关系 */
    emplFamlApi(params) {
        return request.get(`/sap/opu/odata/sap/ZBPM_EMPLOYEE_RESUME_INQUIRY_SRV/P0021Set?$filter=PERNR eq '${params.emplId}'&$format=json`);
    },
    /**地址信息 */
    addressInfoApi(params) {
        return request.get(`/sap/opu/odata/sap/ZBPM_EMPLOYEE_RESUME_INQUIRY_SRV/P0006Set?$filter=PERNR eq '${params.emplId}'&$format=json`);
    },
    /**员工模型(雷达图) */
    empphotoSearchEmpModel(params) {
        return request.get('/api/otr/empphoto/searchEmpModel', params);
    },

    /** 画像 */
    /**家庭成员-画像 */
    emplFamlPortraitApi(params) {
        return request.get(`/sap/opu/odata/sap/ZBPM_EMPLOYEE_PORTRAIT_SRV/P0021Set?$filter=PERNR eq '${params.emplId}'&$format=json`);
    },
    /**工作经历-画像 */
    empphotoSearchWorkExpPortrait(params) {
        return request.get(`/sap/opu/odata/sap/ZBPM_EMPLOYEE_PORTRAIT_SRV/P0023Set?$filter=PERNR eq'${params.emplId}'&$format=json`);
    },
     /**培训信息-画像 */
     trainInfoPortraitApi(params) {
        return request.get(`/sap/opu/odata/sap/ZBPM_EMPLOYEE_PORTRAIT_SRV/P9041Set?$filter=PERNR eq '${params.emplId}'&$format=json`);
    },
};
