import request from '@/api/http/request';

export const talentApi = {
	/**分页查询盘点计划 */
	getOrtolanPageList(params) {
		return request.post('/api/otr/otrplan/page', params);
	},
	/**查询盘点计划详情 */
	getOrtolanDetail(id) {
		return request.get(`/api/otr/otrplan/details/${id}`);
	},
	/**盘点计划提交 */
	getOrtolanCommit(params) {
		return request.post(`/api/otr/otrplan/commit`, params);
	},
	/**盘点计划合并 */
	getOrtolanCombine(params) {
		return request.post('/api/otr/otrplan/combine', params);
	},
	/**获取所属部门数据 */
	getListCompany(params) {
		return request.get('/api/ps/psjob/getTree', params);
	},

	/**得到部门列表 */
	getListDepartment(params) {
		return request.get('/api/base/listDepartment', params);
	},

	/**创建人才盘点 */
	otrplanAdd(params) {
		return request.post('/api/otr/otrplan/add', params);
	},

	/**更新人才盘点 */
	otrplanUpdate(params) {
		return request.post('/api/otr/otrplan/update', params);
	},

	/**分页查询盘点范围人员 */
	otrrangePageList(params) {
		return request.post('/api/otr/otrrange/pageForDTO', params);
	},
	/**所有查询盘点范围人员 */
	otrrangePageListAll(params) {
		return request.get('/api/otr/otrrange/listAll', params);
	},
	/**查询盘点范围人员 */
	otrrangeSerchList(params) {
		return request.get('/api/otr/otrrange/list', params);
	},

	/**得到员工列表 */
	listEmployee(params) {
		return request.get('/api/base/listEmployee', params);
	},

	/**得到员工列表 */
	otrrangeSaveRange(params) {
		return request.post('/api/otr/otrrange/saveRange', params);
	},

	/**单个删除盘点范围 */
	otrrangeDelete(id) {
		return request.post(`/api/otr/otrrange/delete/${id}`);
	},

	/**获取主指标列表 */
	otrtargetList(params) {
		return request.get('/api/otr/otrtarget/list', params);
	},

	/**保存盘点规则 */
	otrruleSave(params) {
		return request.post('/api/otr/otrrule', params);
	},

	/**获取盘点计划详情信息 */
	otrplanGetDetails(id) {
		return request.get(`/api/otr/otrplan/details/${id}`);
	},

	/**人才盘点-分页查询报告 */
	otrsummaryemplList(params) {
		return request.get('/api/otr/otrsummaryempl/page', params);
	},

	/**整体盘点-分页查询报告 */
	otrsummaryplanList(params) {
		return request.get('/api/otr/otrsummaryplan/page', params);
	},

	/**获取盘点结果(对应宫格区间)详情信息 */
	otrblockresultGetDetails(params) {
		return request.get('/api/otr/otrblockresult/getDetails', params);
	},

	/**获取盘点人员结果列表 */
	otrblockresultGetListEmployess(params) {
		return request.get('/api/otr/otrblockresult/item/listEmployess', params);
	},

	/**分页查询调整人员 */
	otradjustGetList(params) {
		return request.get('/api/otr/otradjust/page', params);
	},

	/**整体盘点-发布 */
	otrsummaryplanPublish(params) {
		return request.post('/api/otr/otrsummaryplan/publish', params);
	},

	/**人才盘点-发布 */
	otrsummaryemplPublish(params) {
		return request.post('/api/otr/otrsummaryempl/publish', params);
	},

	/**获取宫格页面规则 */
	otrruleGetRule(params) {
		return request.get('/api/otr/otrrule/getRule', params);
	},

	/**获取指标数据 */
	getOtrtarget(params) {
		return request.get('/api/otr/otrtarget/get', params);
	},

	/**修改盘点规则 */
	otrruleUpdateRule(params) {
		return request.post('/api/otr/otrrule/updateRule', params);
	},

	/**新增宫格设置 */
	otrblockAdd(params) {
		return request.post('/api/otr/otrblock/add', params);
	},
	/**保存所有宫格设置 */
	otrblockSaveMore(params) {
		return request.post('/api/otr/otrblock/saveMore', params);
	},
	/**修改宫格设置 */
	otrblockUpdate(params) {
		return request.post('/api/otr/otrblock/update', params);
	},
	/**获取所有宫格数据 */
	otrblockListAll(params) {
		return request.get('/api/otr/otrblock/listAll', params);
	},


	//盘点结果
	/**生效 */
	otrresultSave(params) {
		return request.post('/api/otr/otrresult/save', params);
	},
	/**根据九宫格ID获取盘点结果 */
	otrresultPageByPlanId(params) {
		return request.post('/api/otr/otrresult/pageByPlanId', params);
	},
	/**盘点结果分页 */
	otrresultPage(params) {
		return request.post('/api/otr/otrresult/page', params);
	},
	/**根据盘点报告id-获取盘点结果分页 */
	otrresultPageForReport(params) {
		return request.post('/api/otr/otrresult/pageForReport', params);
	},
	/**查询盘点结果 */
	otrresultGetResult(params) {
		return request.get('/api/otr/otrresult/getResult', params);
	},
	//盘点报告

	/**获取盘点报告分页列表 */
	reportHeadPage(params) {
		return request.post('/api/report/head/page', params);
	},
	/**审批盘点报告 */
	reportHeadAuditReport(params) {
		return request.post('/api/report/head/auditReport', params);
	},
	/**修改盘点报告 */
	reportHeadUpdate(params) {
		return request.post('/api/report/head/update', params);
	},
	/**获取整体盘点报告详细数据 */
	reportHeadDetail(id) {
		return request.get(`/api/report/head/${id}`);
	},
	/**根据ID获取盘点报告 */
	reportHeadLoadById(params) {
		return request.get('/api/report/head/loadById', params);
	},
	/**获取个人报告 */
	reportHeadLoadPersonalReport(params) {
		return request.postQuery('/api/report/head/loadPersonalReport', params);
	},
	/**生成盘点报告 */
	reportHeadGenerateReportHeadSingle(params) {
		return request.post('/api/report/head/generateReportHeadSingle', params);
	},

	//盘点范围-过程管理
	/**分页查询员工评价结果(过程管理查看明细) */
	otrquestionPage(params) {
		return request.get(`/api/otr/otrquestion/page`, params);
	},
	/**分页查询我要评价的员工列表 */
	otrquestionPageMyAppraise(params) {
		return request.get(`/api/otr/otrquestion/pageMyAppraise`, params);
	},
	/**保存选中的盘点人员数据-盘点范围 */
	otrrangeSaveSelectRange(params) {
		return request.post(`/api/otr/otrrange/saveSelectRange`, params);
	},
	/**验证盘点范围数据是否完整-盘点范围 */
	otrrangeCheckRangeAvail(params) {
		return request.postQuery(`/api/otr/otrrange/checkRangeAvail`, params);
	},


	//结果调整
	/**新增盘点结果调整-盘点结果-结果调整 */
	otrblockadjAdd(params) {
		return request.post(`/api/otr/otrblockadj/add`, params);
	},
	/**分页查询盘点结果调整-盘点结果-结果调整 */
	otrblockadjPage(params) {
		return request.get(`/api/otr/otrblockadj/page`, params);
	},

	//问卷模板
	/**问卷模板-根据盘点计划ID获取 */
	otrQuestionPaperListByPlanId(params) {
		return request.postQuery(`/api/otr/OtrQuestionPaper/listByPlanId`, params);
	},
	/**问卷模板-问卷详情信息 */
	otrQuestionPaperId(id) {
		return request.get(`/api/otr/OtrQuestionPaper/${id}`);
	},
	/**问卷模板-保存 */
	otrQuestionPaperSave(params) {
		return request.post(`/api/otr/OtrQuestionPaper/save`, params);
	},
	/**问卷模板-根据盘点计划ID获取字段相关名称 */
	otrQuestionPaperGetFieldNameByPlanId(planId) {
		return request.get(`/api/otr/OtrQuestionPaper/getFieldNameByPlanId/${planId}`);
	},
};
