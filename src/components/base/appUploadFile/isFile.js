const fileSuffixTypeUtil = {
	matchFileSuffixType(fileName) {
		// 后缀获取
		var suffix = '';
		// 获取类型结果
		var result = '';
		try {
			var fliedArr = fileName.split('.');
			suffix = fliedArr[fliedArr.length - 1];
		} catch (err) {
			suffix = '';
		}
		// fileName无后缀返回 false
		if (!suffix) {
			result = false;
			return result;
		}
		// 图片格式
		var imagist = ['png', 'jpg', 'jpeg', 'bmp', 'gif'];
		// 进行图片匹配
		result = imagist.some(function (item) {
			return item == suffix;
		});
		if (result) {
			result = 'image';
			return result;
		}

		// 匹配文件
		var fileList = ['txt', 'xls', 'xlsx', 'doc', 'docx', 'pdf', 'ppt', 'mp4', 'm2v', 'mkv', 'mp3', 'wav', 'wmv'];
		result = fileList.some(function (item) {
			return item == suffix;
		});
		if (result) {
			result = 'file';
			return result;
		}

		return result;
	},
};

export default fileSuffixTypeUtil;
