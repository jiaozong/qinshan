import index from '@/pages/layout/index.vue'; //布局

const labelLibraryRouter = {
    path: '/',
    name: 'labelLibrary',
    component: index,
    meta: { title: '标签库', icon: 'iconfont icon-gongzuotai', affix: true },
    children: [
        {
            path: 'labelLibrary-labelType',
            name: 'labelType',
            component: () => import('@/pages/labelLibrary/labelType.vue'),
            meta: { title: '标签类型', icon: 'iconfont icon-gongzuotai', affix: true },
        },
        {
            path: 'labelLibrary-labelManage',
            name: 'labelManage',
            component: () => import('@/pages/labelLibrary/labelManage.vue'),
            meta: { title: '标签管理', icon: 'iconfont icon-gongzuotai', affix: true },
        },
        {
            path: 'labelLibrary-labelPlay',
            name: 'labelPlay',
            component: () => import('@/pages/labelLibrary/labelPlay.vue'),
            meta: { title: '打标签', icon: 'iconfont icon-gongzuotai', affix: true },
        },
    ],
};

export default labelLibraryRouter