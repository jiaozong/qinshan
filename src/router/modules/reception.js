import index from '@/pages/layout/index.vue'; //布局

const receptionRouter = {
  path: '/',
  name: 'person',
  component: index,
  meta: { title: '人才', icon: 'iconfont icon-gongzuotai', affix: true },
  children: [{
    name: "talentPortrait",
    path: "talentPortrait",
    component: () => import('@/pages/talentPortrait'),
    meta: { title: '人才画像', icon: 'iconfont icon-gongzuotai', affix: true },
  }, {
    name: "personContrast",
    path: "personContrast",
    component: () => import('@/pages/personContrast'),
    meta: { title: '人才对比', icon: 'iconfont icon-gongzuotai', affix: true },
  },
  {
    name: "searchPerson",
    path: "searchPerson",
    component: () => import('@/pages/searchPerson'),
    meta: { title: '人才搜索', icon: 'iconfont icon-gongzuotai', affix: true },
  },
  ]
};

export default receptionRouter;

