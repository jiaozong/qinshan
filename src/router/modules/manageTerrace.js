import index from '@/pages/layout/index.vue'; //布局

const manageTerraceRouter = {
    path: '/',
    name: 'manage',
    component: index,
    meta: { title: '管理者平台', icon: 'iconfont icon-gongzuotai', affix: true },
    children: [
        {
            path: 'manageTerrace',
            name: 'manageTerrace',
            component: () => import('@/pages/manageTerrace/index.vue'),
            meta: { title: '首页', icon: 'iconfont icon-gongzuotai', affix: true },
        },

    ],
};

export default manageTerraceRouter