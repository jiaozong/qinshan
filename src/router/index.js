import Vue from 'vue'
import Router from 'vue-router'
import labelLibraryRouter from "./modules/labelLibrary.js";
import receptionRouter from "./modules/reception.js";


//
import staffSelfHelpRouter from "./modules/staffSelfHelp.js";
import manageTerraceRouter from "./modules/manageTerrace.js";
import workHRRouter from "./modules/workHR.js";
import dataManageRouter from "./modules/dataManage.js";

Vue.use(Router)

export default new Router({
  routes: [
    // talentReviewRouter,
    // labelLibraryRouter,
    // questionnaireRouter,
    // researchEvaluationRouter,
    // competencyModelRouter,
    // developmentPlanRouter,
    // personnelPortraitRoute,
    
    //
    staffSelfHelpRouter,
    manageTerraceRouter,
    workHRRouter,
    dataManageRouter,
    receptionRouter,
    {
      path: '/login',
      name: 'login',
      component: () => import('@/pages/base/login/index.vue'),
    },
    {
      path: '/404',
      component: () => import('@/pages/base/errorPage/404'),
      hidden: true,
    },
  ]
})
